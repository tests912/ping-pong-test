﻿using PingPong.Main;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace PingPong.UI
{
    public class Score : Singleton<Score>
    {
        #region Piblic fields
        [Header("UI")]
        public Text scoreText;
        public Text bestScoreText;
        #endregion
        #region Props
        public int GetSetScore { get { return _score; } set { _score = value; scoreText.text = value.ToString(); } }
        public int GetBestScore { get { return _bestScore; } private set { _bestScore = value; bestScoreText.text = value.ToString(); } }
        #endregion
        #region Private fields
        private int _score = 0;
        private int _bestScore = 0;
        #endregion

        #region Methods
        private void Start()
        {
            LoadBestScore();
            GetSetScore = GetBestScore;

            GameManager.getInstance.OnGameStart += Init;
            GameManager.getInstance.OnGameEnd += Dispose;
            GameManager.getInstance.OnBallBounce += GameOver;
        }

        private void Init(byte gameMode)
        {
            LoadBestScore();
            SaveAndResetScore();
        }

        private void Dispose()
        {
            LoadBestScore();
            SaveAndResetScore();
        }

        private void GameOver(bool isGameOver)
        {
            if (!isGameOver)
                GetSetScore += 1;
            else
            {
                SaveAndResetScore();
            }
        }

        /// <summary>
        /// Save score to player prefs and then reset it
        /// </summary>
        private void SaveAndResetScore()
        {
            SaveBestScore();
            StartCoroutine(AnimateScoreDownCoroutine());
        }

        IEnumerator AnimateScoreDownCoroutine()
        {
            float _t = 0;
            var _score = GetSetScore;
            while (_t < 1f)
            {
                GetSetScore = Mathf.FloorToInt(Mathf.Lerp(_score, 0, _t));
                _t += Time.deltaTime / 0.5f;
                yield return null;
            }
            yield return new WaitForSeconds(0.2f);
            GetSetScore = 0;
            GameManager.getInstance.OnBallNeedInvoke();
        }
        private async void AnimateScoreDown()
        {
            float _t = 0;
            var _score = GetSetScore;
            while (_t < 1f)
            {
                GetSetScore = (int)Mathf.Lerp(_score, 0, _t);
                _t += Time.deltaTime / 0;


            }
            await Task.Delay(100);
            GetSetScore = 0;
            GameManager.getInstance.OnBallNeedInvoke();
        }

        /// <summary>
        /// Save if player current score is better then last
        /// </summary>
        private void SaveBestScore()
        {
            if (_score >= PlayerPrefs.GetInt("score", 0))
            {
                PlayerPrefs.SetInt("score", _score);
                GetBestScore = _score;
            }
        }

        /// <summary>
        /// Load player score record
        /// </summary>
        private void LoadBestScore()
        {
            GetBestScore = PlayerPrefs.GetInt("score", 0);
        }
        #endregion
    }
}