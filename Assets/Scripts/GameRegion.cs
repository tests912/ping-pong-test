﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PingPong.Main
{
    public class GameRegion : MonoBehaviour
    {
        #region Piblic fields
        [Header("Walls")]
        public GameObject left;
        public GameObject right;
        public GameObject top;
        public GameObject bottom;
        #endregion
    }
}
