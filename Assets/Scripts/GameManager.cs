﻿using System;
using UnityEngine;

namespace PingPong.Main
{
    public class GameManager : Singleton<GameManager>
    {
        #region Piblic fields
        [Header("Options")]
        public Vector2 ballSpeedRange = new Vector2(5, 10);
        public Vector2 ballSizeRange = new Vector2(0.5f, 2f);
        public Color ballColor;
        [Space]
        public Color RacketColor;
        [Header("Prefabs")]
        public GameObject racketPrefab;
        public GameObject ballPrefab;
        [Header("Other")]
        public Canvas canvas;
        public GameRegion gameRegion;
        #endregion
        #region Private fields
        [Header("Dev options")]
        [SerializeField]
        private bool mouseShow = false;
        #endregion
        #region Props
        /// <summary>
        /// Main camera cache
        /// </summary>
        public Camera GetMainCamera { get; private set; }
        public bool GetPauseState { get; private set; } = false;
        #endregion
        #region Events
        /// <summary>
        /// On game session start
        /// </summary>
        public event Action<byte> OnGameStart;
        /// <summary>
        /// On ball needSpawn
        /// </summary>
        public event Action OnBallNeed;
        /// <summary>
        /// On game session pause
        /// </summary>
        public event Action<bool> OnGamePause;
        /// <summary>
        /// On game session end
        /// </summary>
        public event Action OnGameEnd;
        /// <summary>
        /// On game settings changed
        /// </summary>
        public event Action OnGameOptionsChanged;
        /// <summary>
        /// On ball bounce with anything
        /// </summary>
        public event Action<bool> OnBallBounce;
        #endregion

        #region Public methods
        public void OnBallBounceInvoke(bool isGameOver) { if (OnBallBounce != null) OnBallBounce.Invoke(isGameOver); }
        public void OnGameStartInvoke(byte gameMode) { if (OnGameStart != null) OnGameStart.Invoke(gameMode); }
        public void OnBallNeedInvoke() { if (OnBallNeed != null) OnBallNeed.Invoke(); }
        public void OnGamePauseInvoke(bool isRemote) { if (OnGamePause != null) OnGamePause.Invoke(isRemote); }
        public void OnGameOptionsChangedInvoke() { if (OnGameOptionsChanged != null) OnGameOptionsChanged.Invoke(); }
        #endregion
        #region Private methods
        protected override void Awake()
        {
            base.Awake();

            Init();
        }

        private void Start()
        {
            OnGamePause += (bool isRemote) => { GetPauseState = !GetPauseState; };
        }


        private void Init()
        {
            GetMainCamera = Camera.main;
        }

        private void HideMouse()
        {
#if UNITY_EDITOR
            Cursor.visible = mouseShow;
            Cursor.lockState = mouseShow ? CursorLockMode.None : CursorLockMode.Locked;
#else
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
#endif
        }
        #endregion
    }
}