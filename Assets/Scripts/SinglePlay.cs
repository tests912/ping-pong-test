﻿using PingPong.Main;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace PingPong.SinglePlayGame
{
    public class SinglePlay : MonoBehaviour
    {
        #region Props
        /// <summary>
        /// Return rackets or crate it
        /// </summary>
        public List<Racket> GetRackets
        {
            get
            {
                return _rackets != null ? _rackets : _rackets = CreateRackets();
            }
        }
        /// <summary>
        /// Return ball or create it
        /// </summary>
        public Ball GetBall
        {
            get
            {
                return _ball ? _ball : _ball = Instantiate(GameManager.getInstance.ballPrefab, GameManager.getInstance.canvas.transform).GetComponent<Ball>();
            }
        }
        /// <summary>
        /// Game status state
        /// </summary>
        public GameStatus Status { get; private set; } = GameStatus.Stoped;
        #endregion
        #region Private fields
        /// <summary>
        /// cached rocket pos
        /// </summary>
        protected Vector3 _rocketPos = Vector3.zero;

        private Ball _ball;
        private List<Racket> _rackets;
        /// <summary>
        /// cached speed for current session
        /// </summary>
        private float curentSpeed = 0;
        /// <summary>
        /// temp current velocity for pause
        /// </summary>
        private Vector2 _ballVelocityTemp = Vector2.zero;
        #endregion

        #region Public methods
        /// <summary>
        /// Return mouse to world cords clamped between left and right borders
        /// </summary>
        /// <returns></returns>
        public float GetRocketCord()
        {
            return Mathf.Clamp(GameManager.getInstance.GetMainCamera.ScreenToWorldPoint(Input.mousePosition).x,
               GameManager.getInstance.gameRegion.left.transform.position.x,
               GameManager.getInstance.gameRegion.right.transform.position.x);
        }
        #endregion
        #region Private methods
        protected virtual void Start()
        {
            GameManager.getInstance.OnGameStart += Init;
            GameManager.getInstance.OnBallNeed += OnBallNeed;
            GameManager.getInstance.OnGameEnd += Dispose;
            GameManager.getInstance.OnBallBounce += OnBounce;
            GameManager.getInstance.OnGamePause += GamePause;
            GameManager.getInstance.OnGameOptionsChanged += OptionsChanged;
        }

        protected virtual void Update()
        {
            if (Status == GameStatus.Started)
            {
                MoveRackets(GetRackets);
            }
        }

        protected virtual void OnApplicationQuit()
        {
            Dispose();
        }

        /// <summary>
        /// Create ball and rackets
        /// </summary>
        protected virtual void Init(byte gameMode)
        {
            GetBall.boxCollider.enabled = true;
            GetBall.circleCollider.enabled = true;
            GetBall.rigidbody2D.simulated = true;

            GetBall.SetBall(Random.Range(GameManager.getInstance.ballSizeRange.x, GameManager.getInstance.ballSizeRange.y), GameManager.getInstance.ballColor);

            curentSpeed = Random.Range((int)GameManager.getInstance.ballSpeedRange.x, (int)GameManager.getInstance.ballSpeedRange.y);
            GameOver();

            Status = GameStatus.Started;
        }

        /// <summary>
        /// Move rackets list to mouse pos
        /// </summary>
        /// <param name="rackets">Rackets list</param>
        protected virtual void MoveRackets(List<Racket> rackets)
        {
            _rocketPos.x = GetRocketCord();

            for (int i = 0; i < rackets.Count; i++)
            {
                _rocketPos.y = rackets[i].transform.position.y;
                rackets[i].transform.position = _rocketPos;
            }
        }

        protected virtual void OnBounce(bool isGameOver)
        {
            if (Status == GameStatus.Stoped) return;
            if (isGameOver) { GameOver(); return; }

            GetBall.rigidbody2D.velocity = Vector2.Min(new Vector2(GetBall.rigidbody2D.velocity.x > 0 ? 1 : -1, GetBall.rigidbody2D.velocity.y > 0 ? 1 : -1), GetBall.rigidbody2D.velocity.normalized) * curentSpeed;
            GetBall.rigidbody2D.angularVelocity = (GetBall.rigidbody2D.velocity.x > 0 ? -1 : 1) * curentSpeed * 10;

        }

        protected virtual void OptionsChanged()
        {
            if (Status == GameStatus.Stoped) return;
            GetBall.image.color = GameManager.getInstance.ballColor;
        }

        protected virtual void GamePause(bool isRemote)
        {
            if (Status == GameStatus.Stoped) return;

            if (Status != GameStatus.Paused)
            {
                Status = GameStatus.Paused;

                _ballVelocityTemp = GetBall.rigidbody2D.velocity;
                GetBall.rigidbody2D.velocity = Vector2.zero;
            }
            else
            {
                Status = GameStatus.Started;
                GetBall.rigidbody2D.velocity = _ballVelocityTemp;
            }
        }

        /// <summary>
        /// Distroy ball and rackets
        /// </summary>
        protected virtual void Dispose()
        {
            if (Status == GameStatus.Stoped) return;
            Status = GameStatus.Stoped;

            Destroy(_ball.gameObject);
            foreach (var racket in _rackets)
            {
                Destroy(racket.gameObject);
            }
        }

        private void GameOver()
        {
            GetBall.rigidbody2D.velocity = Vector2.zero;
        }

        private void OnBallNeed()
        {
            GetBall.rigidbody2D.position = Vector2.zero;

            GetBall.rigidbody2D.velocity = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f)).normalized * curentSpeed;
        }

        private List<Racket> CreateRackets()
        {
            var _l = new List<Racket>();
            for (int i = 0; i < 2; i++)
            {
                _l.Add(Instantiate(GameManager.getInstance.racketPrefab, GameManager.getInstance.gameRegion.bottom.transform.position, Quaternion.identity, GameManager.getInstance.canvas.transform).GetComponent<Racket>());
                _l[_l.Count - 1].image.color = GameManager.getInstance.RacketColor;
                _l[_l.Count - 1].transform.SetAsFirstSibling();
            }
            _l[0].transform.position = GameManager.getInstance.gameRegion.bottom.transform.position;
            _l[0].rectTransform.localPosition = new Vector2(_l[0].rectTransform.localPosition.x, _l[0].rectTransform.localPosition.y + _l[0].rectTransform.rect.height / 2);
            _l[1].transform.position = GameManager.getInstance.gameRegion.top.transform.position;
            _l[1].rectTransform.localPosition = new Vector2(_l[1].rectTransform.localPosition.x, _l[1].rectTransform.localPosition.y - _l[1].rectTransform.rect.height / 2);
            return _l;
        }
        #endregion
    }
}