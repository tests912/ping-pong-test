﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PingPong.Main
{
    public class Ball : MonoBehaviour
    {
        #region Piblic fields
        public Rigidbody2D rigidbody2D;
        public Image image;
        public CircleCollider2D circleCollider;
        public BoxCollider2D boxCollider;
        #endregion

        #region Public methods
        public void SetBall(float size, Color color)
        {
            transform.localScale = Vector3.one * size;
            image.color = color;
        }

        public void SetBall(float size, Color color, Vector2 _pos)
        {
            transform.localScale *= size;
            image.color = color;
            transform.position = _pos;
        }
        #endregion
        #region Private methods
        /// <summary>
        /// The timer to prevent overly frequent calls
        /// </summary>
        float _lastCollisionTime = 0;
        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (_lastCollisionTime < Time.time - 0.1f)
            {
                _lastCollisionTime = Time.time;
                GameManager.getInstance.OnBallBounceInvoke(false);
            }
        }
        /// <summary>
        /// The timer to prevent overly frequent calls
        /// </summary>
        float _lastTriggerTime = 0;
        private void OnTriggerExit2D(Collider2D collision)
        {
            if (_lastTriggerTime < Time.time - 0.2f)
            {
                _lastTriggerTime = Time.time;
                GameManager.getInstance.OnBallBounceInvoke(true);
            }
        }
        #endregion
    }
}