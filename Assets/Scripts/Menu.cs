﻿using PingPong.Main;
using UnityEngine;
using UnityEngine.UI;

namespace PingPong.UI
{
    public class Menu : MonoBehaviour
    {
        #region Piblic fields
        [Header("UI")]
        public GameObject mainMenu;
        public GameObject inGame;
        public GameObject options;
        [Header("Inputs")]
        public Button startButton;
        public Button networkButton;
        public Button customizeButton;
        public Button optionsButton;
        public Button pauseButton;
        public Slider colorSlider;
        #endregion
        #region Private fields
        private bool _isPause = false;
        #endregion

        #region Public methods
        public void OnSliderMouseUp()
        {
            PlayerPrefs.SetFloat("color", colorSlider.value);
        }
        #endregion
        #region Private methods
        private void Start()
        {
            GameManager.getInstance.OnGameStart += StartGame;
            GameManager.getInstance.OnGameEnd += Dispose;
            GameManager.getInstance.OnGamePause += Pause;

            startButton.onClick.AddListener(() => { GameManager.getInstance.OnGameStartInvoke((byte)GameMods.Singleplayer); });
            networkButton.onClick.AddListener(() => { GameManager.getInstance.OnGameStartInvoke((byte)GameMods.Mutliplayer); });
            pauseButton.onClick.AddListener(() => { GameManager.getInstance.OnGamePauseInvoke(false); });
            optionsButton.onClick.AddListener(() => { options.SetActive(!options.activeSelf); });
            colorSlider.onValueChanged.AddListener((float _val) =>
            {
                GameManager.getInstance.ballColor = Color.HSVToRGB(_val, 1, 1);
                GameManager.getInstance.OnGameOptionsChangedInvoke();
            });

            colorSlider.value = PlayerPrefs.GetFloat("color", 0.12f);

            Dispose();
        }

        private void StartGame(byte gameMode)
        {
            _isPause = false;
            PauseTurn(_isPause);
            inGame.SetActive(true);
        }

        private void Dispose()
        {
            _isPause = false;
            PauseTurn(_isPause);
            mainMenu.SetActive(true);
            inGame.SetActive(false);
       
        }

        private void Pause(bool isRemote)
        {
            _isPause = !_isPause;

            PauseTurn(_isPause);
        }

        private void PauseTurn(bool _onOff)
        {
            if (_isPause) pauseButton.image.color = new Color(1, 1, 1, 1);
            else
            {
                options.SetActive(false);
                pauseButton.image.color = new Color(1, 1, 1, 0.4f);
            }

            mainMenu.SetActive(_isPause);
        }
        #endregion
    }
}
