﻿using H.Socket.IO;
using PingPong.Main;
using Misc.Utility;
using PingPong.SinglePlayGame;
using PingPong.UI;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace PingPong.Multiplayer
{
    public class NetPlay : SinglePlay
    {
        #region Piblic fields
        [Header("Client options")]
        public string ip = "127.0.0.1:2222";
        public float sendEvery = 0.05f;//send every x seconds
        public AnimationCurve interpolationCurve = AnimationCurve.Linear(0, 0, 1, 1);
        [Header("UI")]
        public Text statusText;
        public string connectingText = "Connecting...";
        public string searchingText = "Searching players...";
        public string disconnectedText = "Disconnected";
        public string foundText = "Player connected!";
        public string pausedText = "Pause";
        #endregion
        #region Props
        public SocketIoClient SocketIoClient { get; private set; } = new SocketIoClient();
        public NetGameStatus NetStatus { get; private set; } = NetGameStatus.Disconnected;
        #endregion
        #region Data types
        /// <summary>
        /// Message types
        /// </summary>
        public enum SocketEvent
        {
            ROOM_FOUND,//who is room owner
            RACKET_POS,  //racket pos 
            POS, // ball and racket pos because package headers size > package 2x body
            OPTIONS, //ball color and scale
            PAUSE, //pause
        }
        /// <summary>
        /// Recive message model, used for parse
        /// </summary>
        public class MsgModel
        {
            public float? RacketPosX { get; set; }//Racket position
            public Vector3? BallPosRot { get; set; }//Ball position and rotation cords={x,y} angle=z
            public bool? RoomOwner { get; set; }//is current player are room owner
            public int Score { get; set; }//game score
        }

        /// <summary>
        /// Recive message model for parse options args
        /// </summary>
        public class OptionsMsgModel
        {
            public float? BallScale { get; set; }//Ball scale
            public Color BallColor { get; set; }//Ball color
        }

        public struct MsgOutModel
        {
            public string msgType;
            public dynamic body;

            public MsgOutModel(string _msgType, dynamic _body)
            {
                msgType = _msgType;
                body = _body;
            }
        }
        #endregion
        #region Private fields
        private bool _isRoomOwner = false;
        private Queue<Action> _actionsQueue = new Queue<Action>();
        private Queue<MsgOutModel> _sendingsQueue = new Queue<MsgOutModel>();
        private float _globalIntepolationTime = 0;
        private Vector3[] ballCords = new Vector3[2]; //0 - pre last, 1 - last
        private float[] racketCords = new float[2];
        private Vector3 ballRotarionCache = Vector3.zero;
        private Vector2 ballPositionCache = Vector2.zero;
        private Vector2 racketPositionCache = Vector2.zero;
        #endregion


        #region Methods
        protected override void Start()
        {
            base.Start();


            SocketIoClient.Connected += (sender, e) => _actionsQueue.Enqueue(() => OnConnected());
            SocketIoClient.Disconnected += (sender, e) => _actionsQueue.Enqueue(() => { NetStatus = NetGameStatus.Disconnected; statusText.text = disconnectedText; });

            SocketIoClient.On<MsgModel>(SocketEvent.ROOM_FOUND.ToString(), (msg) => _actionsQueue.Enqueue(() => RoomFound(msg.RoomOwner)));
            SocketIoClient.On<MsgModel>(SocketEvent.RACKET_POS.ToString(), (msg) => _actionsQueue.Enqueue(() => SetPos(msg.RacketPosX)));
            SocketIoClient.On<MsgModel>(SocketEvent.POS.ToString(), (msg) => _actionsQueue.Enqueue(() => { SetPos(msg.RacketPosX, msg.BallPosRot); SetScore(msg.Score); }));
            SocketIoClient.On<OptionsMsgModel>(SocketEvent.OPTIONS.ToString(), (msg) => _actionsQueue.Enqueue(() => SetOptions(msg.BallScale, msg.BallColor)));
            SocketIoClient.On(SocketEvent.PAUSE.ToString(), () => _actionsQueue.Enqueue(() => GameManager.getInstance.OnGamePauseInvoke(true)));
        }

        protected override void Update()
        {
            base.Update();

            Resive();

            if (NetStatus == NetGameStatus.Disconnected || Status == GameStatus.Stoped) return;

            SendPos();
            InterpolationCoords();
        }

        protected override void Init(byte gameMode)
        {
            if (gameMode != (byte)GameMods.Mutliplayer)
            {
                Dispose();
                base.Init(gameMode);
                return;
            }

            if (NetStatus == NetGameStatus.Disconnected)
            {
                statusText.text = connectingText;
                SocketIoClient.ConnectAsync(new Uri($"ws://{ip}/"));
            }
            else
            {
                Reconnect();

            }
        }

        protected override void OnApplicationQuit()
        {
            base.OnApplicationQuit();

            Dispose();
        }

        protected override void GamePause(bool isRemote)
        {
            base.GamePause(isRemote);

            if (!isRemote) SendPause();

            statusText.text = Status == GameStatus.Paused ? pausedText : Score.getInstance.GetSetScore.ToString();
        }

        protected override void OptionsChanged()
        {
            base.OptionsChanged();

            SendOptions();
        }

        protected override void MoveRackets(List<Racket> rackets)
        {
            if (NetStatus == NetGameStatus.Disconnected)
            {
                base.MoveRackets(rackets);
                return;
            }

            _rocketPos.x = GetRocketCord();
            _rocketPos.y = GetRackets[0].transform.position.y;
            GetRackets[0].transform.position = _rocketPos;

        }

        protected override void Dispose()
        {
            /*  Task.Run(() =>
              {
                  SocketIoClient.DisconnectAsync();
              });*/
             SocketIoClient.DisconnectAsync();
            _isRoomOwner = false;
            _actionsQueue.Clear();
        }


        private void OnConnected()
        {
            NetStatus = NetGameStatus.Connected;
            statusText.text = searchingText;
            SendingJob();
        }

        private async void Reconnect()
        {
            Dispose();
            await Task.Delay(1000);
            Init((byte)GameMods.Mutliplayer);
        }

        private async void RoomFound(bool? isRoomOwner)
        {
            statusText.text = foundText;
            await Task.Delay(500);

            _isRoomOwner = isRoomOwner ?? false;

            base.Init((byte)GameMods.Mutliplayer);
            if (_isRoomOwner)
            {
                SendOptions();
            }

            GetBall.boxCollider.enabled = _isRoomOwner;
            GetBall.circleCollider.enabled = _isRoomOwner;
            GetBall.rigidbody2D.simulated = _isRoomOwner;
        }

        private void SetOptions(float? _BallScale, Color _BallColor)
        {
            if (_BallScale.HasValue)
                GetBall.transform.localScale = Vector3.one * _BallScale.Value;
            GetBall.image.color = _BallColor;
        }

        private void SetScore(int _score)
        {
            if(Status!=GameStatus.Paused)
                Score.getInstance.GetSetScore = _score;
        }

        private void SetPos(float? _racketPosX, Vector3? _ballPosNRot = null)
        {
            _globalIntepolationTime = 0;

            var _screenScaleX = Mathf.Abs(GameManager.getInstance.gameRegion.right.transform.position.x - GameManager.getInstance.gameRegion.left.transform.position.x);
            var _screenScaleY = Mathf.Abs(GameManager.getInstance.gameRegion.bottom.transform.position.y - GameManager.getInstance.gameRegion.top.transform.position.y);

            racketCords[1] = -((_racketPosX ?? 0) * _screenScaleX - (_screenScaleX * 0.5f));
            racketCords[0] = GetRackets[1].transform.position.x;

            if (_ballPosNRot.HasValue)
            {
                ballCords[1].x = -(_ballPosNRot.Value.x * _screenScaleX - (_screenScaleX * 0.5f));
                ballCords[1].y = (_ballPosNRot.Value.y * _screenScaleY - (_screenScaleY * 0.5f));
                ballCords[1].z = -_ballPosNRot.Value.z;

                ballCords[0].x = Math.Abs(ballCords[1].x - GetBall.transform.position.x) > 1 ? ballCords[1].x : GetBall.transform.position.x;
                ballCords[0].y = Math.Abs(ballCords[1].y - GetBall.transform.position.y) > 1 ? ballCords[1].y : GetBall.transform.position.y;
                ballCords[0].z = GetBall.transform.rotation.eulerAngles.z;
            }
        }

        float _timer = 0;
        private void SendPos()
        {
            if (Time.time - sendEvery > _timer)
            {
                _timer = Time.time;
                if (_isRoomOwner)
                    Send(SocketEvent.POS, new
                    {
                        RacketPosX = Utils.GetPos01(GetRackets[0].transform.position.x, GameManager.getInstance.gameRegion.right.transform.position.x, GameManager.getInstance.gameRegion.left.transform.position.x),
                        BallPosRot = new
                        {
                            x = Utils.GetPos01(GetBall.rigidbody2D.position.x, GameManager.getInstance.gameRegion.right.transform.position.x, GameManager.getInstance.gameRegion.left.transform.position.x),
                            y = Utils.GetPos01(GetBall.rigidbody2D.position.y, GameManager.getInstance.gameRegion.bottom.transform.position.y, GameManager.getInstance.gameRegion.top.transform.position.y),
                            z = GetBall.rigidbody2D.rotation
                        },
                        Score = Score.getInstance.GetSetScore
                    });
                else
                    Send(SocketEvent.RACKET_POS, new
                    {
                        RacketPosX = Utils.GetPos01(GetRackets[0].transform.position.x, GameManager.getInstance.gameRegion.right.transform.position.x, GameManager.getInstance.gameRegion.left.transform.position.x)
                    });
            }
        }

        private void SendOptions()
        {
            Send(SocketEvent.OPTIONS, new
            {
                BallScale = GetBall.transform.localScale.y,
                BallColor = new
                {
                    a = GameManager.getInstance.ballColor.a,
                    r = GameManager.getInstance.ballColor.r,
                    g = GameManager.getInstance.ballColor.g,
                    b = GameManager.getInstance.ballColor.b,
                }
            });
        }

        private void SendPause()
        {
            Send(SocketEvent.PAUSE, null);
        }


        private void SendingJob()
        {
            if (NetStatus == NetGameStatus.Connected)
                Task.Factory.StartNew(async () =>
                {
                    while (NetStatus == NetGameStatus.Connected)
                    {
                        if (_sendingsQueue.Count > 0)
                        {
                            var _msg = _sendingsQueue.Dequeue();
                            await SocketIoClient.Emit(_msg.msgType, _msg.body);
                        }
                    }

                }, TaskCreationOptions.LongRunning);
        }

        private void Send(SocketEvent msgType, dynamic body)
        {
            _sendingsQueue.Enqueue(new MsgOutModel(msgType.ToString(), body));
        }

        private void Resive()
        {
            if (_actionsQueue.Count > 50) _actionsQueue.Clear();
            if (_actionsQueue.Count > 0) _actionsQueue.Dequeue()();
            if (_actionsQueue.Count > 2)
                for (int i = 0; i < _actionsQueue.Count; i++)
                {
                    _actionsQueue.Dequeue()();
                }
        }


        private void InterpolationCoords()
        {
            racketPositionCache.x = Mathf.Lerp(racketCords[0], racketCords[1], interpolationCurve.Evaluate(_globalIntepolationTime));
            racketPositionCache.y = GetRackets[1].transform.position.y;
            GetRackets[1].transform.position = racketPositionCache;

            if (_isRoomOwner) return;

            ballPositionCache = Vector2.Lerp(ballCords[0], ballCords[1], interpolationCurve.Evaluate(_globalIntepolationTime));
            ballRotarionCache.z = Mathf.Lerp(ballCords[0].z, ballCords[1].z, interpolationCurve.Evaluate(_globalIntepolationTime));


            GetBall.transform.rotation = Quaternion.Euler(ballRotarionCache);
            GetBall.transform.position = ballPositionCache;//_BallPosNRotP2;

            _globalIntepolationTime += Time.deltaTime / sendEvery;
        }
        #endregion
    }
}