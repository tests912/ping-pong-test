﻿using UnityEngine;

namespace Misc.Hacks
{
    public class ColliderHack : MonoBehaviour
    {
        #region Public fields
        public BoxCollider2D collider;
        public CircleCollider2D colliderCirccle;
        public RectTransform rectTransform;
        #endregion

        #region Methods
        private void Start()
        {
            if (collider)
                collider.size = new Vector2(rectTransform.rect.width, rectTransform.rect.height);
            if (colliderCirccle)
                colliderCirccle.radius = rectTransform.rect.width / 2;
        }
        #endregion
    }
}