﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntiRotate : MonoBehaviour
{
    #region Fields
    [Header("Options")]
    public float angle = 0;
    #endregion

    // Update is called once per frame
    void Update()
    {
        transform.rotation = Quaternion.Euler(0, 0, angle);
    }
}
