﻿

#region Enums
public enum GameStatus : byte
{
    Started,
    Paused,
    Stoped
}

public enum NetGameStatus : byte
{
    Connected,
    Disconnected
}

enum GameMods : byte
{
    Singleplayer,
    Mutliplayer
}
#endregion

