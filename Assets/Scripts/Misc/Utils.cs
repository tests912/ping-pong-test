﻿using UnityEngine;

namespace Misc.Utility
{
    static public class Utils
    {
        #region Methods
        /// <summary>
        /// Return random int between min and max +- minVal
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="minVal"></param>
        /// <returns></returns>
        static public int RandomRange(int min, int max, int minVal)
        {
            var _r = UnityEngine.Random.Range(min, max);

            return _r > 0 ? _r + minVal : _r - minVal;
        }

        /// <summary>
        /// Return random float between min and max +- minVal
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="minVal"></param>
        /// <returns></returns>
        static public float RandomRange(float min, float max, float minVal)
        {
            var _r = UnityEngine.Random.Range(min, max);
            return _r > 0 ? _r + minVal : _r - minVal;
        }


        static public float GetPos01(float value, float maxPoint, float minpoint)
        {
            return Mathf.Abs((minpoint - value) /
                            (maxPoint - minpoint));
        }

        static public Vector3 GetPos01(Vector3 value, float maxPoint, float minpoint)
        {
            return new Vector3(GetPos01(value.x, maxPoint, minpoint), GetPos01(value.y, maxPoint, minpoint), GetPos01(value.z, maxPoint, minpoint));
        }

        static public Vector2 GetPos01(Vector2 value, float maxPoint, float minpoint)
        {
            return new Vector2(GetPos01(value.x, maxPoint, minpoint), GetPos01(value.y, maxPoint, minpoint));
        }
        #endregion
    }
}
